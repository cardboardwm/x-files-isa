//
// Created by alex on 7/30/20.
//

#ifndef X_FILES_ISA_CONFIG_H
#define X_FILES_ISA_CONFIG_H

// Configuration file

const int WIDTH_MULTIPLIER = 10;
const int HEIGHT_MULTIPLIER = 4;

static const std::unordered_map<sf::Keyboard::Key, uint8_t> input_mapping = {
        {sf::Keyboard::Num1, 0x0},
        {sf::Keyboard::Num2, 0x1},
        {sf::Keyboard::Num3, 0x2},
        {sf::Keyboard::Q, 0x3},
        {sf::Keyboard::W, 0x4},
        {sf::Keyboard::E, 0x5},
        {sf::Keyboard::A, 0x6},
        {sf::Keyboard::S, 0x7},
        {sf::Keyboard::D, 0x8},
        {sf::Keyboard::Z, 0x9},
        {sf::Keyboard::C, 0xa},
        {sf::Keyboard::Num4, 0xb},
        {sf::Keyboard::R, 0xc},
        {sf::Keyboard::F, 0xd},
        {sf::Keyboard::V, 0xe},
};

static const std::unordered_map<Pixel, sf::Color> color_mapping = {
        {BLACK, sf::Color(0, 0, 0)},
        {YELLOW, sf::Color(255, 255, 0)},
        {ORANGE, sf::Color(255, 100, 3)},
        {RED, sf::Color(255, 0, 0)},
        {MAGENTA, sf::Color(242, 8, 132)},
        {PURPLE, sf::Color(71, 0, 165)},
        {BLUE, sf::Color(0, 0, 255)},
        {CYAN, sf::Color(2, 171, 234)},
        {GREEN, sf::Color(31, 183, 20)},
        {DARK_GREEN, sf::Color(0, 100, 18)},
        {BROWN, sf::Color(86, 44, 5)},
        {TAN, sf::Color(144, 113, 58)},
        {MEDIUM_GRAY, sf::Color(128, 128, 128)},
        {DARK_GRAY, sf::Color(64, 64, 64)},
        {WHITE, sf::Color(255, 255, 255)}
};

#endif //X_FILES_ISA_CONFIG_H
