cmake_minimum_required(VERSION 3.9)
project(emulator)

set(CMAKE_CXX_STANDARD 20)

find_package(SFML COMPONENTS graphics REQUIRED)
find_package(Threads REQUIRED)

add_executable(emulator src/main.cpp src/instructions.cpp)
target_link_libraries(emulator sfml-graphics Threads::Threads portaudio_static)
target_include_directories(emulator PRIVATE include/)