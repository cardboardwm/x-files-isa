//
// Created by alex on 7/30/20.
//

#ifndef X_FILES_ISA_INSTRUCTION_H
#define X_FILES_ISA_INSTRUCTION_H

#include <array>
#include <cstdint>

struct Platform;

using Opcode = std::uint32_t;

struct Instruction {
    unsigned int id; // first 1 or 2 bytes; -1 == null
    void(*execute)(Platform&, Opcode); // executes instruction
};

using InstructionTable = std::array<Instruction, 256>;

template<size_t N>
constexpr InstructionTable generate_instruction_table(std::array<Instruction, N> instructions)
{
    InstructionTable instruction_table;

    for(const auto& instruction: instructions)
    {
        instruction_table[instruction.id] = instruction;
    }

    return instruction_table;
}

#endif //X_FILES_ISA_INSTRUCTION_H
