//
// Created by alex on 7/30/20.
//

#ifndef X_FILES_ISA_INSTRUCTIONS_H
#define X_FILES_ISA_INSTRUCTIONS_H

#include <cstdint>

struct Platform;
using Opcode = std::uint32_t;

/*
 * FFFF = memory address
 * II | I = instruction
 * DD = Data (1 byte)
 * R = register (1 nibble)
 */

/*
 * single nibble id instructions: 0x0 - 0xB
 * double nibble id instructions: 0xC0 - 0xFF
 */

//IRFFFF; I = 0x0
void load(Platform&, Opcode); // load memory into register

//IRFFFF; I = 0x1
void store(Platform&, Opcode); // store register into memory

//IRDD; I = 0x2
void load_constant(Platform&, Opcode); // load constant into register

//IIRRRP; I = 0xDC
void load_special_register(Platform&,  Opcode); // loads a special register into 2 general registers

//IIRR; I = 0xDE
void load_special_register_high_byte(Platform&, Opcode); // loads the high byte of a special register into a general one

//IIRR; I = 0xDF
void load_special_register_low_byte(Platform&, Opcode); // loads the high byte of a special register into a general one

//IIRR; I = 0xDA
void store_special_register_high_byte(Platform&, Opcode); // stores the value of general register into the high byte of the special register

//IIRR; I = 0xDB
void store_special_register_low_byte(Platform&, Opcode); // stores the value of general register into the high byte of the special register

//IRRR; I = 0x5
void load_memory_from_registers(Platform&, Opcode); //interprets 2 registers as an address and loads into a third one

//IRRR; I = 0x6
void store_memory_from_registers(Platform&, Opcode); //interprets 2 registers as an address and stores a third register into the memory

//IIRR; II = 0xC0
void add(Platform&, Opcode);

//IIRR; II = 0xC1
void sub(Platform&, Opcode);

//IIRR; II = 0xC2
void div(Platform&, Opcode);

//IIRR; II = 0xC3
void mul(Platform&, Opcode);

//IIRR; II = 0xC9
void mod(Platform&, Opcode);

//IIRR; II = 0xC4
void and_(Platform&, Opcode);

//IIRR; II = 0xC5
void or_(Platform&, Opcode);

//IIRR; II = 0xC6
void xor_(Platform&, Opcode);

//IIRR; II = 0xC7
void left_shift(Platform&, Opcode);

//IIRR; II = 0xC8
void right_shift(Platform&, Opcode);

//IR; I = 0x3
void increment(Platform&, Opcode);

//IR; I = 0x4
void decrement(Platform&, Opcode);

//IIFFFF; II = 0xD0
void jump(Platform&, Opcode);

//IIRRFFFF; II = 0xD1
void jump_equal(Platform&, Opcode);

//IIRRFFFF; II = 0xD2
void jump_lesser(Platform&, Opcode);

//IIRRFFFF; II = 0xD3
void jump_greater(Platform&, Opcode);

//IIRRFFFF; II = 0xD4
void jump_not_equal(Platform&, Opcode);

//IIRRFFFF; II = 0xD5
void jump_lesser_equal(Platform&, Opcode);

//IIRRFFFF; II = 0xD6
void jump_greater_equal(Platform&, Opcode);

//IIRRFFFF; I = 0xE0
void draw_bw_sprite(Platform&, Opcode);
// draws a "black white sprite", replacing colors with colors dictated by register r5

//IIRRFFFF; II = 0xE1
void draw_sprite(Platform&, Opcode); // draws a grayscale sprite

//II; II = 0xE2
void clear_screen(Platform&, Opcode);

//IIRP; II = 0xE4
void clear_color_screen(Platform&, Opcode);

//II; II = 0xE3
void frame(Platform&, Opcode); // sends frame signal to the main processing unit

//II; II = 0xEF
void load_keys(Platform&, Opcode); // load keys into io register

//IIRP; II = 0xEE
void random(Platform&, Opcode); // load random number into register

//II; II = 0xED
void send_notes(Platform&, Opcode); // send the audio register to the sound system

//IIRF; II = 0xFE
void print_register(Platform&, Opcode); //prints register

//IIFFFF; II = 0xFD
void print_memory(Platform&, Opcode); // prints memory

//II; II = 0xFF
void illegal_instruction(Platform&, Opcode);

#endif //X_FILES_ISA_INSTRUCTIONS_H
