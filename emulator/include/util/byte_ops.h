//
// Created by alex on 7/30/20.
//

#ifndef X_FILES_ISA_BYTE_OPS_H
#define X_FILES_ISA_BYTE_OPS_H

#include <cstdint>
#include <array>

static uint32_t combine(std::array<uint8_t, 4> data)
{
    uint32_t r = 0;
    for(auto x: data) {
        r = (r << 8u) + x;
    }

    return r;
}

static std::uint16_t extract_2_bytes(std::uint32_t data, unsigned position, unsigned pad)
{
    position = (position + pad) * 4;
    return ((0xFFFFu << position) & data) >> position;
}

static std::uint8_t extract_nibble(std::uint32_t data, unsigned position, unsigned pad)
{
    position = (position + pad) * 4;
    return ((0xFu << position) & data) >> position;
}

static std::uint8_t extract_byte(std::uint32_t data, unsigned position, unsigned pad)
{
    position = (position + pad) * 4;
    return ((0xFFu << position) & data) >> position;
}

static uint32_t to_little_endian(uint32_t data)
{
    return ((data >> 24u) & 0xffu) +
           ((data << 8u) & 0xff0000u) +
           ((data >> 8u) & 0xff00u) +
           ((data << 24u) & 0xff000000u);
}

#endif //X_FILES_ISA_BYTE_OPS_H
