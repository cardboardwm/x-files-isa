//
// Created by alex on 7/30/20.
//

#ifndef X_FILES_ISA_PLATFORM_H
#define X_FILES_ISA_PLATFORM_H

#include <Instruction.h>
#include <instructions.h>
#include <Graphics.h>

#include <atomic>
#include <random>
#include <cstdlib>
#include <csignal>

[[noreturn]]
static void fault(const char* message)
{
    fprintf(stderr, "fault: %s\n", message);
    raise(SIGTRAP);
    exit(-1);
}

const int VIDEO_RAM = 3840;
const int RAM = 128;
const int ROM = 4096;

struct Memory {
    uint8_t memory[ROM + RAM + VIDEO_RAM];
    /*
     * ROM: 0 - 0x1000
     * RAM: 0x1000 - 0x1080
     * VRAM: 0x1080 - 0x2080
     */

    void write(uint32_t address, uint8_t data)
    {
        if(address < 0x1000 || address >= 0x2080) {
            fault("invalid write address");
        }

        memory[address] = data;
    }

    uint8_t read(uint32_t address)
    {
        if(address < 0x2080)
        {
            return memory[address];
        }

        fault("invalid read address");
    }
};

struct Platform {
    uint8_t registers[6]{}; // register names: A, B, C, D, E, F
    uint16_t program_counter{}, io{}, audio{}; // pc is 6, io register is 7, audio is 8
    Memory memory{};

    /***/
    std::atomic<uint8_t> keys[16]{};
    std::atomic<bool> render_flag = false;
    std::atomic<bool> quit_flag = false;
    std::atomic<bool> pause_flag = false;
    uint32_t graphics_buffer[SCREEN_HEIGHT][SCREEN_WIDTH];
    std::mt19937 random_engine;
    std::uniform_int_distribution<uint8_t> uniform_distribution{};
    std::atomic<int> instructions_per_frame;
    std::atomic<long> min_instruction_time;
    std::atomic<long> average_instruction_time;
    std::atomic<long> max_instruction_time;

    /**/
    std::atomic<std::uint16_t> notes;
    double time = 0;
};

static uint16_t& map_special_register(Platform& platform, int id)
{
    if(id == 6)
        return platform.program_counter;
    else if(id == 7)
        return platform.io;
    else if(id == 8)
        return platform.audio;

    fault("invalid special register");
}

static uint8_t& map_register(Platform& platform, int id)
{
    if(id >= 0 && id <= 5)
        return platform.registers[id];

    fault("invalid general register");
}

inline InstructionTable instruction_table = generate_instruction_table(std::array {
        Instruction {0x0, load},
        Instruction {0x1, store},
        Instruction {0x2, load_constant},
        Instruction {0xDC, load_special_register},
        Instruction {0xDE, load_special_register_high_byte},
        Instruction {0xDF, load_special_register_low_byte},
        Instruction {0xDA, store_special_register_high_byte},
        Instruction {0xDB, store_special_register_low_byte},
        Instruction {0x5, load_memory_from_registers},
        Instruction {0x6, store_memory_from_registers},
        Instruction {0xC0, add},
        Instruction {0xC1, sub},
        Instruction {0xC2, div},
        Instruction {0xC3, mul},
        Instruction {0xC9, mod},
        Instruction {0xC4, and_},
        Instruction {0xC5, or_},
        Instruction {0xC6, xor_},
        Instruction {0xC7, left_shift},
        Instruction {0xC8, right_shift},
        Instruction {0x3, increment},
        Instruction {0x4, decrement},
        Instruction {0xD0, jump},
        Instruction {0xD1, jump_equal},
        Instruction {0xD2, jump_lesser},
        Instruction {0xD3, jump_greater},
        Instruction {0xD4, jump_not_equal},
        Instruction {0xD5, jump_lesser_equal},
        Instruction {0xD6, jump_greater_equal},
        Instruction {0xE0, draw_bw_sprite},
        Instruction {0xE1, draw_sprite},
        Instruction {0xE2, clear_screen},
        Instruction {0xE4, clear_color_screen},
        Instruction {0xE3, frame},
        Instruction {0xEF, load_keys},
        Instruction {0xEE, random},
        Instruction {0xED, send_notes},
        Instruction {0xFE, print_register},
        Instruction {0xFD, print_memory},
        Instruction {0xFF, illegal_instruction},
});

const int FRAME_INSTRUCTION_ID = 0xE3;

inline Instruction decode(uint32_t opcode)
{
    unsigned int id = (opcode & 0xF0000000u) >> 28u;
    if(id > 0xBu) {
        id = (opcode & 0xFF000000) >> 24u;
    }

    return instruction_table[id];
}

#endif //X_FILES_ISA_PLATFORM_H
