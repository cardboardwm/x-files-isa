//
// Created by alex on 7/31/20.
//

#ifndef X_FILES_ISA_AUDIO_H
#define X_FILES_ISA_AUDIO_H

#include <portaudio.h>
#include <Platform.h>

const int SAMPLE_RATE = 44100;
const float SAMPLE_FREQUENCY = 1.f / SAMPLE_RATE;

static float get_frequency_from_key(int key)
{
    static float middleOctave[12] = {261.6256, 277.1826, 293.6648, 311.1270, 329.6276, 349.2282, 369.9944, 391.9954, 415.3047, 440.0000, 466.1638, 493.8833};

    int octave = key / 12;
    int note = key % 12;
    float p = octave < 5 ? 1.f / (1 << (5 - octave)) : 1 << (octave - 5);

    return p * middleOctave[note];
}

static float wave_generator(float frequency, double time, double phase, float level)
{
    double full_period_time = 1.f / frequency;
    double local_time = fmod(time, full_period_time);

    double saw_wave = level * ((local_time / full_period_time) * 2. - 1.0);
    double sin_wave = level * sin(2.f * M_PI * frequency * time + phase);

    return saw_wave + sin_wave;
}


int synth_callback(
    const void*,
    void* output_buffer,
    unsigned long samples_per_buffer,
    const PaStreamCallbackTimeInfo* time_info,
    PaStreamCallbackFlags status_flags,
    void* user_data
)
{
    auto* out = static_cast<float*> (output_buffer);
    auto* platform = static_cast<Platform*>(user_data);

    std::uint16_t note_data = platform->notes;
    std::uint8_t keys[] = {
        static_cast<uint8_t>(note_data & 0xFFu),
        static_cast<uint8_t>(note_data >> 8u)
    };

    int key_size = (keys[0] != 0) + (keys[1] != 0);

    for(unsigned long i = 0; i < samples_per_buffer; i++)
    {
        float a = 0.0f;
        for(auto key: keys)
        {
            if(key != 0)
            {
                a += (1.f / key_size) * wave_generator(get_frequency_from_key(key), platform->time, 0.5f, 0.7f);
            }
            platform->time += SAMPLE_FREQUENCY;
        }

        *out++ = a;
        *out++ = a;
    }

    return 0;
}

#endif //X_FILES_ISA_AUDIO_H
