//
// Created by alex on 7/30/20.
//

#ifndef X_FILES_ISA_GRAPHICS_H
#define X_FILES_ISA_GRAPHICS_H

const int SCREEN_WIDTH = 40;
const int SCREEN_HEIGHT = 192;
const int SPRITE_WIDTH = 16;
const int SPRITE_HEIGHT = 16;

enum Pixel: uint8_t
{
    BLACK = 0x0,
    YELLOW = 0x1,
    ORANGE = 0x2,
    RED = 0x3,
    MAGENTA = 0x4,
    PURPLE = 0x5,
    BLUE = 0x6,
    CYAN = 0x7,
    GREEN = 0x8,
    DARK_GREEN = 0x9,
    BROWN = 0xA,
    TAN = 0xB,
    TRANSPARENT = 0xC,
    MEDIUM_GRAY = 0xD,
    DARK_GRAY = 0xE,
    WHITE = 0xF
};

static Pixel load_pixel(uint8_t* vram, int x, int y)
{
    int index = y * SCREEN_WIDTH + x;
    auto address = index / 2;

    if(index % 2 == 0) // high nibble
        return static_cast<Pixel>(vram[address] >> 4u);
    else // low nibble
        return static_cast<Pixel>(vram[address] & 0xFu);
}

static void store_pixel(uint8_t* vram, int x, int y, Pixel pixel)
{
    int index = y * SCREEN_WIDTH + x;
    auto address = index / 2;

    if(index % 2 == 0) // high nibble
        vram[address] = (vram[address] & 0xFu) + (static_cast<uint8_t>(pixel) << 4u);
    else // low nibble
        vram[address] = (vram[address] & 0xF0u) + static_cast<uint8_t>(pixel);
}

static Pixel load_sprite_pixel(uint8_t* vram, int x, int y)
{
    int index = y * SPRITE_WIDTH + x;
    auto address = index / 2;

    if(index % 2 == 0) // high nibble
        return static_cast<Pixel>(vram[address] >> 4u);
    else // low nibble
        return static_cast<Pixel>(vram[address] & 0xFu);
}

static void store_sprite_pixel(uint8_t* vram, int x, int y, Pixel pixel)
{
    int index = y * SPRITE_WIDTH + x;
    auto address = index / 2;

    if(index % 2 == 0) // high nibble
        vram[address] = (vram[address] & 0xFu) + (static_cast<uint8_t>(pixel) << 4u);
    else // low nibble
        vram[address] = (vram[address] & 0xF0u) + static_cast<uint8_t>(pixel);
}


#endif //X_FILES_ISA_GRAPHICS_H
