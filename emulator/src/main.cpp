#include <stdio.h>
#include <fstream>
#include <queue>
#include <cstdint>
#include <thread>
#include <chrono>

#include <Platform.h>
#include <Graphics.h>
#include <Audio.h>
#include <util/byte_ops.h>

#include <SFML/Graphics.hpp>

#include "../Config.h"

void load_rom_file(Platform& platform, const char* filename)
{
    std::ifstream f{filename};
    f.read(reinterpret_cast<char *>(platform.memory.memory), ROM);
}

void emulator_thread(Platform* platform)
{
    int instructions_per_frame = 1;
    long min_instruction_time = 100;
    long max_instruction_time = 0;
    double average_instruction_time = 0;
    while(!platform->quit_flag)
    {
        uint32_t opcode = combine({
            platform->memory.read(platform->program_counter),
            platform->memory.read(platform->program_counter + 1),
            platform->memory.read(platform->program_counter + 2),
            platform->memory.read(platform->program_counter + 3)
        });

        if(!platform->pause_flag)
        {
            auto start = std::chrono::high_resolution_clock::now();
            Instruction instruction = decode(opcode);
            instruction.execute(*platform, opcode);
            instructions_per_frame++;
            auto stop = std::chrono::high_resolution_clock::now();

            auto duration = std::chrono::duration<long, std::nano>(stop - start).count();
            min_instruction_time = std::min(duration, min_instruction_time);
            max_instruction_time = std::max(duration, max_instruction_time);
            average_instruction_time =
                    (duration + (instructions_per_frame - 1) * average_instruction_time) / instructions_per_frame;

            if(instruction.id == FRAME_INSTRUCTION_ID)
            {
                while(platform->render_flag && !platform->quit_flag) ;

                for(int y = 0; y < SCREEN_HEIGHT; y++)
                {
                    for(int x = 0; x < SCREEN_WIDTH; x++)
                        platform->graphics_buffer[y][x] =
                                to_little_endian(
                                        color_mapping.at(load_pixel(&platform->memory.memory[ROM + RAM], x, y)).toInteger());
                }

                platform->instructions_per_frame = instructions_per_frame;
                platform->min_instruction_time = min_instruction_time;
                platform->max_instruction_time = max_instruction_time;
                platform->average_instruction_time = average_instruction_time;

                platform->render_flag = true;
                while(platform->render_flag && !platform->quit_flag) ;
                instructions_per_frame = 1;
                min_instruction_time = 100;
                max_instruction_time = 0;
                average_instruction_time = 0;
            }
        }
        else
        {
            platform->instructions_per_frame = instructions_per_frame;
            platform->min_instruction_time = min_instruction_time;
            platform->max_instruction_time = max_instruction_time;
            platform->average_instruction_time = average_instruction_time;
        }
    }
}

int main(int argv, char *argc[]) {
    if (argv != 2) {
        printf("usage: %s rom_file", argc[0]);
    }

    Platform platform;
    load_rom_file(platform, argc[1]);

    sf::RenderWindow window(
            sf::VideoMode(
                    SCREEN_WIDTH * WIDTH_MULTIPLIER,
                    SCREEN_HEIGHT * HEIGHT_MULTIPLIER),
            "x-files-isa emulator");

    PaError error = Pa_Initialize();
    if(error != paNoError)
        printf("PortAudio error: %s\n", Pa_GetErrorText(error)), exit(-1);

    PaStream* stream;

    error = Pa_OpenDefaultStream(
            &stream,
            0,
            2,
            paFloat32,
            SAMPLE_RATE,
            512,
            synth_callback,
            &platform);

    if(error != paNoError)
        printf("PortAudio error: %s\n", Pa_GetErrorText(error)), exit(-1);

    error = Pa_StartStream(stream);
    if(error != paNoError)
        printf("PortAudio error: %s\n", Pa_GetErrorText(error)), exit(-1);

    sf::Font font;
    font.loadFromFile("emulator/resources/roboto_mono.ttf");

    sf::Texture screen{};
    screen.create(SCREEN_WIDTH, SCREEN_HEIGHT);
    sf::Sprite sprite = sf::Sprite{screen};

    bool render_debug_hud = false;
    sf::Texture ram_view{};
    ram_view.create(8, 16);
    uint32_t ram_view_buffer[128];
    int instruction_per_frame = 0;
    int max_instruction_per_frame = 0;
    long min_instruction_time;
    long average_instruction_time;
    long max_instruction_time;

    std::thread emulator(emulator_thread, &platform);

    window.setFramerateLimit(60);
    while (window.isOpen())
    {
        sf::Event event{};
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                if (auto it = input_mapping.find(event.key.code); it != input_mapping.end())
                    platform.keys[it->second] = true;

                if(event.key.code == sf::Keyboard::F1)
                    render_debug_hud = !render_debug_hud;

                if(render_debug_hud && event.key.code == sf::Keyboard::P) // pause action
                    platform.pause_flag = !platform.pause_flag;
            }
            else if (event.type == sf::Event::KeyReleased)
            {
                if (auto it = input_mapping.find(event.key.code); it != input_mapping.end())
                    platform.keys[it->second] = false;
            }
        }

        if(platform.render_flag)
        {
            screen.update(reinterpret_cast<uint8_t *>(platform.graphics_buffer));
            screen.setSrgb(false);
            sprite = sf::Sprite{screen};
            sprite.scale({WIDTH_MULTIPLIER, HEIGHT_MULTIPLIER});
            instruction_per_frame = platform.instructions_per_frame;
            min_instruction_time = platform.min_instruction_time;
            average_instruction_time = platform.average_instruction_time;
            max_instruction_time = platform.max_instruction_time;
            max_instruction_per_frame = std::max(max_instruction_per_frame, instruction_per_frame);
            platform.render_flag = false;
        }

        window.clear();
        window.draw(sprite);
        if(render_debug_hud)
        {
            for(int i = 0; i < 128; i++)
            {
                ram_view_buffer[i] =
                        to_little_endian(
                                (platform.memory.memory[ROM + i] ? sf::Color::Blue : sf::Color::Red).toInteger());
            }

            ram_view.update(reinterpret_cast<uint8_t *>(ram_view_buffer));
            sf::Sprite ram_view_sprite{ram_view};
            auto min_multiplier = std::min(WIDTH_MULTIPLIER, HEIGHT_MULTIPLIER);
            ram_view_sprite.scale({static_cast<float>(min_multiplier), static_cast<float>(min_multiplier)});

            window.draw(ram_view_sprite);

            sf::Text lines[5];

            for(int i = 0; i < 5; i++)
            {
                lines[i].setFont(font);
                lines[i].setCharacterSize(12);
                lines[i].setFillColor(sf::Color::White);
                lines[i].setPosition(0, 16.f * min_multiplier + i * 12.f);
            }

            lines[0].setString("max i/f: " + std::to_string(max_instruction_per_frame));
            lines[1].setString("i/f: " + std::to_string(instruction_per_frame));
            lines[2].setString("min i: " + std::to_string(min_instruction_time) + "ns");
            lines[3].setString("max i: " + std::to_string(max_instruction_time) + "ns");
            lines[4].setString("avg i: " + std::to_string(average_instruction_time) + "ns");

            for(auto& text: lines)
                window.draw(text);
        }
        window.display();
    }

    platform.quit_flag = true;
    emulator.join();

    error = Pa_StopStream(stream);
    if(error != paNoError)
        printf("PortAudio error: %s\n", Pa_GetErrorText(error)), exit(-1);

    error = Pa_Terminate();
    if(error != paNoError)
        printf("PortAudio error: %s\n", Pa_GetErrorText(error)), exit(-1);

    return 0;
}
