//
// Created by alex on 7/30/20.
//

#include <instructions.h>

#include <Platform.h>
#include <Graphics.h>
#include <util/byte_ops.h>

#include <stdio.h>
#include <string.h>

void load(Platform& platform, Opcode opcode)
{
    auto reg = extract_nibble(opcode, 4, 2);
    auto address = extract_2_bytes(opcode, 0, 2);

    map_register(platform, reg) = platform.memory.read(address);

    platform.program_counter += 3;
}

void store(Platform& platform, Opcode opcode) 
{
    auto reg = extract_nibble(opcode, 4, 2);
    auto address = extract_2_bytes(opcode, 0, 2);

    platform.memory.write(address, map_register(platform, reg));

    platform.program_counter += 3;
}

void load_constant(Platform& platform, Opcode opcode) 
{
    auto reg = extract_nibble(opcode, 2, 4);
    auto data = extract_byte(opcode, 0, 4);

    map_register(platform, reg) = data;

    platform.program_counter += 2;
}

void load_special_register(Platform& platform, Opcode opcode) 
{
    auto special_register = extract_nibble(opcode, 1, 2);
    auto reg1 = extract_nibble(opcode, 3, 2);
    auto reg2 = extract_nibble(opcode, 2, 2);

    uint16_t value = map_special_register(platform, special_register);
    map_register(platform, reg1) = (0xFF00u & value) >> 8u;
    map_register(platform, reg2) = (0xFFu & value);

    platform.program_counter += 3;
}

void load_special_register_high_byte(Platform& platform, Opcode opcode) 
{
    auto special_register = extract_nibble(opcode, 1, 4);
    auto reg = extract_nibble(opcode, 0, 4);

    uint16_t value = map_special_register(platform, special_register);
    map_register(platform, reg) = (0xFF00u & value) >> 8u;

    platform.program_counter += 2;
}

void load_special_register_low_byte(Platform& platform, Opcode opcode) 
{
    auto special_register = extract_nibble(opcode, 1, 4);
    auto reg = extract_nibble(opcode, 0, 4);

    uint16_t value = map_special_register(platform, special_register);
    map_register(platform, reg) = 0xFFu & value;

    platform.program_counter += 2;
}

void store_special_register_high_byte(Platform& platform, Opcode opcode)
{
    auto special_register = extract_nibble(opcode, 1, 4);
    auto reg = extract_nibble(opcode, 0, 4);

    uint16_t& value = map_special_register(platform, special_register);
    value = (map_register(platform, reg) << 8u) + (value & 0xFFu);

    platform.program_counter += 2;
}

void store_special_register_low_byte(Platform& platform, Opcode opcode)
{
    auto special_register = extract_nibble(opcode, 1, 4);
    auto reg = extract_nibble(opcode, 0, 4);

    uint16_t& value = map_special_register(platform, special_register);
    value = ((value >> 8u) << 8u) + (map_register(platform, reg) & 0xFFu);

    platform.program_counter += 2;
}

void load_memory_from_registers(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 2, 4);
    auto reg2 = extract_nibble(opcode, 1, 4);
    auto reg3 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg3) = platform.memory.read(
            (static_cast<uint16_t>(map_register(platform, reg1)) << 8u) + map_register(platform, reg2));

    platform.program_counter += 2;
}

void store_memory_from_registers(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 2, 4);
    auto reg2 = extract_nibble(opcode, 1, 4);
    auto reg3 = extract_nibble(opcode, 0, 4);

    platform.memory.write(
            (static_cast<uint16_t>(map_register(platform, reg1)) << 8u) + map_register(platform, reg2),
            map_register(platform, reg3)
    );

    platform.program_counter += 2;
}

void add(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) += map_register(platform, reg2);

    platform.program_counter += 2;
}

void sub(Platform& platform, Opcode opcode) 
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) -= map_register(platform, reg2);

    platform.program_counter += 2;
}

void div(Platform& platform, Opcode opcode) 
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) /= map_register(platform, reg2);

    platform.program_counter += 2;
}

void mul(Platform& platform, Opcode opcode) 
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) *= map_register(platform, reg2);

    platform.program_counter += 2;
}

void mod(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) %= map_register(platform, reg2);

    platform.program_counter += 2;
}


void and_(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) &= map_register(platform, reg2);

    platform.program_counter += 2;
}

void or_(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) |= map_register(platform, reg2);

    platform.program_counter += 2;
}

void xor_(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) ^= map_register(platform, reg2);

    platform.program_counter += 2;
}

void left_shift(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) <<= map_register(platform, reg2);

    platform.program_counter += 2;
}

void right_shift(Platform& platform, Opcode opcode)
{
    auto reg1 = extract_nibble(opcode, 1, 4);
    auto reg2 = extract_nibble(opcode, 0, 4);

    map_register(platform, reg1) >>= map_register(platform, reg2);

    platform.program_counter += 2;
}

void increment(Platform& platform, Opcode opcode) 
{
    auto reg = extract_nibble(opcode, 0, 6);

    map_register(platform, reg) ++;

    platform.program_counter += 1;
}

void decrement(Platform& platform, Opcode opcode) 
{
    auto reg = extract_nibble(opcode, 0, 6);

    map_register(platform, reg) --;

    platform.program_counter += 1;
}

void jump(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 2);
    platform.program_counter = address;
}

void jump_equal(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg1 = extract_nibble(opcode, 5, 0);
    auto reg2 = extract_nibble(opcode, 4, 0);

    if(map_register(platform, reg1) == map_register(platform, reg2))
        platform.program_counter = address;
    else platform.program_counter += 4;
}

void jump_lesser(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg1 = extract_nibble(opcode, 5, 0);
    auto reg2 = extract_nibble(opcode, 4, 0);

    if(map_register(platform, reg1) < map_register(platform, reg2))
        platform.program_counter = address;
    else platform.program_counter += 4;
}

void jump_greater(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg1 = extract_nibble(opcode, 5, 0);
    auto reg2 = extract_nibble(opcode, 4, 0);

    if(map_register(platform, reg1) > map_register(platform, reg2))
        platform.program_counter = address;
    else platform.program_counter += 4;
}

void jump_not_equal(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg1 = extract_nibble(opcode, 5, 0);
    auto reg2 = extract_nibble(opcode, 4, 0);

    if(map_register(platform, reg1) != map_register(platform, reg2))
        platform.program_counter = address;
    else platform.program_counter += 4;
}

void jump_lesser_equal(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg1 = extract_nibble(opcode, 5, 0);
    auto reg2 = extract_nibble(opcode, 4, 0);

    if(map_register(platform, reg1) <= map_register(platform, reg2))
        platform.program_counter = address;
    else platform.program_counter += 4;
}

void jump_greater_equal(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg1 = extract_nibble(opcode, 5, 0);
    auto reg2 = extract_nibble(opcode, 4, 0);

    if(map_register(platform, reg1) >= map_register(platform, reg2))
        platform.program_counter = address;
    else platform.program_counter += 4;
}

void draw_bw_sprite(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg_x = extract_nibble(opcode, 5, 0);
    auto reg_y = extract_nibble(opcode, 4, 0);

    auto x = map_register(platform, reg_x);
    auto y = map_register(platform, reg_y);
    auto color = map_register(platform, 4); // r5

    uint8_t* vram = &platform.memory.memory[RAM + ROM];

    auto black = static_cast<Pixel>(0xFu & color);
    auto white = static_cast<Pixel>(color >> 4u);

    int byte = address;
    unsigned bit = 0;
    for(int dy = 0; dy < SPRITE_HEIGHT; dy++)
    {
        if(dy + y < 0 || dy + y >= SCREEN_HEIGHT)
            continue ;

        for(int dx = 0; dx < SPRITE_WIDTH; dx++)
        {
            if(dx + x < 0 || dx + x >= SCREEN_WIDTH)
                continue ;

            if(bit == 8u)
                byte ++;
            auto r = platform.memory.read(byte) & (0xFu << (7u - bit)) >> (7u - bit);

            if(r)
                store_pixel(vram, x + dx, y + dy, white);
            else store_pixel(vram, x + dx, y + dy, black);

            bit++;
        }
    }

    platform.program_counter += 4;
}

void draw_sprite(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 0);
    auto reg_x = extract_nibble(opcode, 5, 0);
    auto reg_y = extract_nibble(opcode, 4, 0);

    auto x = map_register(platform, reg_x);
    auto y = map_register(platform, reg_y);

    uint8_t* vram = &platform.memory.memory[RAM + ROM];

    for(int dy = 0; dy < SPRITE_HEIGHT; dy++)
    {
        if(dy + y < 0 || dy + y >= SCREEN_HEIGHT)
            continue ;

        for(int dx = 0; dx < SPRITE_WIDTH; dx++)
        {
            if(dx + x < 0 || dx + x >= SCREEN_WIDTH)
                continue ;

            if(auto pixel = load_sprite_pixel(&platform.memory.memory[address], dx, dy);
                pixel != TRANSPARENT)
            {
                store_pixel(vram, dx + x, dy + y, pixel);
            }
        }
    }

    platform.program_counter += 4;
}

void clear_screen(Platform& platform, Opcode opcode)
{
    memset(&platform.memory.memory[RAM + ROM], 0, VIDEO_RAM);
    platform.program_counter += 1;
}

void clear_color_screen(Platform& platform, Opcode opcode)
{
    auto reg = extract_nibble(opcode, 1, 4);

    uint8_t value = map_register(platform, reg) & 0xFu;
    memset(&platform.memory.memory[RAM + ROM], (value << 4u) + value, VIDEO_RAM);
    platform.program_counter += 2;
}

void frame(Platform& platform, Opcode opcode)
{
    platform.program_counter += 1; // no-op
}

void load_keys(Platform& platform, Opcode opcode)
{
    platform.io = 0;

    for(unsigned int i = 0; i < 16; i++)
        platform.io += (!!platform.keys[i]) << i;

    platform.program_counter += 1;
}

void random(Platform& platform, Opcode opcode)
{
    auto reg = extract_nibble(opcode, 1, 4);

    map_register(platform, reg) = platform.uniform_distribution(platform.random_engine);

    platform.program_counter += 2;
}

void send_notes(Platform& platform, Opcode opcode)
{
    platform.notes = platform.audio;

    platform.program_counter += 1;
}

void print_register(Platform& platform, Opcode opcode)
{
    auto reg = extract_nibble(opcode, 1, 4);

    int data = map_register(platform, reg);
    printf("r%d = %d (0x%x)\n", static_cast<int>(reg + 1), data, data);

    platform.program_counter += 2;
}

void print_memory(Platform& platform, Opcode opcode)
{
    auto address = extract_2_bytes(opcode, 0, 2);

    int data = platform.memory.read(address);
    printf("r[0x%x] = %d (0x%x)\n", static_cast<int>(address), data, data);

    platform.program_counter += 3;
}

void illegal_instruction(Platform& platform, Opcode)
{
    fault("illegal instruction");
}

