.date
$TITLU "DOOM"
# Constante
# X_POS a1000, Y_POS a1001, COUNTER_OBSTACOLE a1002, NR_OBSTACOLE a1003
$FPS_NATIV /3c/
$FPS_TINTA /0f/
$SPRITE /cc cc cc c1 cc cc cc cc cc c1 11 11 cc cc cc cc cc 11 bb bb bc cc cc cc cb bb bb bb bb cc cc cc bb ff bb bf fb cc cc cc bb f4 bb bf 4b bc cc cc bb bb bb bb bb bc cc cc bb 2b bb bb bb bc cc cc bb 22 bb b2 2b cc cc cc bb b2 22 22 bb cc cc cc cb bb bb bb bb cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc/
$BAD_SPRITE /c5 4c cc c4 cc cc cc cc c5 5c cc c5 5c cc cc cc 55 cc cc cc 5c cc cc cc 55 03 33 33 05 cc cc cc 33 30 33 30 33 cc cc cc 33 33 33 33 33 cc cc cc 33 00 33 30 03 3c cc cc 33 0f 33 3f 03 3c cc cc 33 33 33 33 33 3c cc cc 33 33 33 33 33 33 cc cc 33 30 00 00 33 33 cc cc c3 30 33 30 33 3c cc cc cc c3 33 33 33 cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc/
$DOOM_UNU /30 00 00 00 00 00 00 00 03 33 33 00 00 03 33 00 03 00 00 30 00 30 00 30 03 00 00 03 03 00 00 03 03 03 33 03 03 03 33 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 03 00 33 03 03 03 30 03 03 00 03 03 03 00 00 30 00 30 00 30 03 00 03 00 00 03 03 00 03 00 30 00 00 00 30 00 03 03 00 00 00 00 00 00 03 30 00 00 00 00 00 00/
$DOOM_DOI /00 00 00 00 00 00 00 03 00 03 33 00 00 00 00 33 00 30 00 30 00 33 00 33 03 00 00 03 03 03 03 03 03 03 33 03 03 03 03 03 03 03 03 03 03 00 33 03 03 03 03 03 03 00 00 03 03 03 03 03 03 00 00 03 03 03 03 03 03 00 30 03 03 03 30 03 03 00 30 03 03 03 00 03 03 03 30 03 00 30 00 30 00 30 30 03 00 03 03 00 00 00 03 03 00 00 30 00 00 00 00 33 00 00 00 00 00 00 00 03 00 00 00 00 00 00 00 00/
$PENTA_UNU /cc cc cc cc cc cc 00 00 cc cc cc cc c0 00 cc cc cc cc cc c0 0c cc cc cc cc cc cc 0c cc cc cc cc cc cc c0 cc cc cc cc cc cc cc 00 cc cc cc cc cc cc c0 c0 00 cc cc cc cc cc 0c cc 0c 00 cc cc cc cc 0c cc 0c cc 00 cc cc c0 cc cc c0 cc cc 00 cc c0 cc cc c0 cc cc cc 00 c0 cc cc c0 cc cc cc 00 0c cc cc cc 0c cc 00 cc 0c cc cc cc 0c 00 cc cc 0c cc cc cc 00 cc cc cc 0c cc cc 00 c0 cc cc cc/
$PENTA_DOI /00 00 cc cc cc cc cc cc cc cc 00 00 cc cc cc cc cc cc cc cc 0c cc cc cc cc cc cc cc c0 cc cc cc cc cc cc cc cc 0c cc cc cc cc cc cc cc 00 cc cc cc cc cc cc 00 0c 0c cc cc cc cc 00 c0 cc c0 cc cc cc 00 cc c0 cc c0 cc cc 00 cc cc 0c cc cc 0c 00 cc cc cc 0c cc cc 0c 00 cc cc c0 cc cc cc 0c cc 00 cc c0 cc cc cc c0 cc cc 00 0c cc cc cc c0 cc cc cc 00 cc cc cc c0 cc cc cc 0c 00 cc cc c0/
$PENTA_TREI /0c cc 00 cc c0 cc cc cc 0c 00 cc cc cc 0c cc cc 00 00 00 00 00 00 00 00 0c cc cc cc cc c0 cc cc c0 cc cc cc cc c0 cc cc c0 cc cc cc cc c0 cc cc c0 cc cc cc cc cc 0c cc cc 0c cc cc cc cc 0c cc cc 0c cc cc cc cc c0 cc cc c0 cc cc cc cc c0 cc cc cc 0c cc cc cc c0 cc cc cc c0 cc cc cc cc 0c cc cc cc 0c cc cc cc 0c cc cc cc c0 cc cc cc c0 cc cc cc cc 00 00 cc c0 cc cc cc cc cc cc 00 00/
$PENTA_PATRU /cc cc c0 cc cc 00 cc c0 cc cc c0 cc cc cc 00 c0 00 00 00 00 00 00 00 00 cc cc 0c cc cc cc cc c0 cc c0 cc cc cc cc cc 0c cc c0 cc cc cc cc cc 0c cc c0 cc cc cc cc cc 0c cc 0c cc cc cc cc c0 cc cc 0c cc cc cc cc c0 cc c0 cc cc cc cc cc 0c cc c0 cc cc cc cc c0 cc cc 0c cc cc cc cc 0c cc cc 0c cc cc cc c0 cc cc cc cc cc cc cc 0c cc cc cc cc cc 00 00 cc cc cc cc 00 00 cc cc cc cc cc cc/

# len 4
$MUZICA_BUNA /3a 3a 3a 33/

.cod
SALTAC r1 255
XAU r2 r2
!bucla_intro
SARMAE r2 r1 !bucla_joc
SALTAC r3 4
SALTAC r4 80
DESSPIR r3 r4 $DOOM_UNU
SALTAC r3 20
DESSPIR r3 r4 $DOOM_DOI
CADRU
SALTAC r3 1
ADN r2 r3
SARI !bucla_intro
!bucla_joc
# sare cadre pentru a reduce FPS-ul
SALTA r1 $FPS_NATIV
SALTA r2 $FPS_TINTA
IMP r1 r2
SALTAC r3 1
SCA r1 r3
SALTAC r2 0
SARMAE r2 r1 !sf_bucla_fps
CADRU
INC r2
!sf_bucla_fps

# counter-ul de la a1002 variaza intre 0 si 100. cand atinge 100, spawnam ceva
SALTA r1 a1002
SALTAC r2 100
SALTAC r3 93
SARMIE r1 r3 !not_play_noise
SALTAC r3 95
SARMAE r1 r3 !not_play_noise
SALTAC r6 48
SARI !noise_played
!not_play_noise
SALTAC r6 0
!noise_played
AFRE r6
TINESSUS r9 r6
BAGASISTEMU
SARIE r1 a2 !not_enemy_spawn
NIMEREALA r1
NIMEREALA r2
SALTAC r3 40
SALTAC r4 192
MOD r1 a3
MOD r2 a4
# la a1003 este numarul de obstacole, pe care il vom incrementa
SALTA r3 a1003
SALTAC r4 1
ADN r3 r4
TINE r3 a1003

# incepand de la a1004 sunt perechi x, y cu pozitiile obstacolelor
SALTAC r4 16
SALTAC r5 03
SALTAC r6 2
MUL r3 r6
ADN r5 r3
TINEMEM r4 r5 r2

SALTAC r6 1
SCA r5 r6
TINEMEM r4 r5 r1
!not_enemy_spawn
# incrementam counter-ul
SALTA r1 a1002
SALTAC r2 1
ADN r1 r2
SALTAC r2 101
MOD r1 r2
TINE r1 a1002

# mutam personajul in functie de input
SALTA r1 a1000
SALTA r2 a1001

INTST
SALTASJOS r3 r8

SALTAC r6 1

SALTAC r4 2
SI r4 r3
SALTAC r5 2
SARIE r4 r5 !nu_sus
SCA r2 r6
!nu_sus
SALTAC r4 16
SI r4 r3
SALTAC r5 16
SARIE r4 r5 !nu_jos
ADN r2 r6
!nu_jos
SALTAC r4 8
SI r4 r3
SALTAC r5 8
SARIE r4 r5 !nu_stanga
SCA r1 r6
!nu_stanga
SALTAC r4 32
SI r4 r3
SALTAC r5 32
SARIE r4 r5 !nu_dreapta
ADN r1 r6
!nu_dreapta

# stocam noile coordonate si desenam
TINE r1 a1000
TINE r2 a1001
STERGE
DESSPIR r1 r2 $SPRITE
# desenam obstacolele
SALTA r6 a1003
!obstacole
SALTAC r5 0
SARE r6 r5 !end_obstacole

SALTAC r1 16
SALTAC r2 03
SALTAC r3 2
XAU r5 r5
ADN r5 r6
MUL r5 r3
ADN r2 r5
# r3 e liber
SALTAMEM r1 r2 r3
SALTAC r5 1
SCA r2 r5
SALTAMEM r1 r2 r4
DESSPIR r4 r3 $BAD_SPRITE
SALTAC r5 1
SCA r6 r5
# pana aici am desenat inamicul si am decrementat interatorul r6

# verifica coliziune cu inamicul
# multiline
SALTA r1 a1000
SALTA r2 a1001
SARMI r1 r4 !not_inside
SARMI r2 r3 !not_inside
SALTAC r1 9
ADN r4 r1
ADN r3 r1
SALTA r1 a1000
SARMA r1 r4 !not_inside
SARMA r2 r3 !not_inside
SARI !dead
!not_inside
SARI !obstacole
!end_obstacole
CADRU
SARI !bucla_joc

!dead
SALTAC r6 3
STERGECULOARE r6
SALTAC r3 16
SALTAC r1 4
SALTAC r2 40
DESSPIR r1 r2 $PENTA_UNU
ADN r1 r3
DESSPIR r1 r2 $PENTA_DOI
ADN r2 r3
DESSPIR r1 r2 $PENTA_PATRU
SALTAC r1 4
DESSPIR r1 r2 $PENTA_TREI
CADRU
SARI !dead
