.date
$TITLU "rain demo"
$BLUE /6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc 6c cc cc cc cc cc cc cc/
$CYAN /7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc 7c cc cc cc cc cc cc cc/
$GRAY /dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc dc cc cc cc cc cc cc cc/

.cod
!initializare
SALTAC r1 0
SALTAC r4 16
!bucla_initializare
XAU r2 r2
ADN r2 r1
SALTAC r3 5
MUL r2 r3
SALTAC r3 192
TINEMEM r4 r2 r3

INC r1
SALTAC r3 25
SARMI r1 r3 !bucla_initializare

!simulare
STERGE
SALTAC r1 0
SALTAC r6 16
!bucla
XAU r2 r2
ADN r2 r1
SALTAC r3 5
MUL r2 r3

SALTAMEM r6 r2 r3
SALTAC r4 192
SARMI r3 r4 !altfel

# pozitie
NIMEREALA r3
SALTAC r4 11
SALTAC r5 3
MUL r3 r5
MOD r3 r4
TINEMEM r6 r2 r3

# coloana
SALTAC r4 40
NIMEREALA r3
NIMEREALA r4
ADN r3 r4
SALTAC r4 40
MOD r3 r4
INC r2
TINEMEM r6 r2 r3

# culoare
SALTAC r4 3
NIMEREALA r3
MOD r3 r4
INC r2
INC r2
TINEMEM r6 r2 r3

# viteaza
XAU r5 r5
ADN r5 r3
SALTAC r4 2
ADN r4 r3
NIMEREALA r3
MOD r3 r4
DEC r2
TINEMEM r6 r2 r3

# wait
SALTAC r3 0
INC r2
INC r2
TINEMEM r6 r2 r3

!altfel
XAU r2 r2
ADN r2 r1
SALTAC r3 5
MUL r2 r3

SALTAC r3 4
ADN r2 r3
SALTAMEM r6 r2 r5
INC r5
TINEMEM r6 r2 r5

SALTAC r3 2
SCA r2 r3
SALTAMEM r6 r2 r4

SARMI r5 r4 !deseneaza
SALTAC r5 0
INC r2
INC r2
TINEMEM r6 r2 r5
SALTAC r3 4
SCA r2 r3
SALTAMEM r6 r2 r5
INC r5
TINEMEM r6 r2 r5
INC r2
INC r2

!deseneaza
DEC r2
DEC r2
SALTAMEM r6 r2 r5
INC r2
SALTAMEM r6 r2 r4

INC r2
INC r2
SALTAMEM r6 r2 r3

SALTAC r2 0
SARE r3 r2 !culoare_0
SALTAC r2 1
SARE r3 r2 !culoare_1
DESSPIR r4 r5 $BLUE
SARI !sfarsit
!culoare_1
DESSPIR r4 r5 $GRAY
SARI !sfarsit
!culoare_0
DESSPIR r4 r5 $CYAN
!sfarsit

INC r1
SALTAC r3 25
SARMI r1 r3 !bucla

SALTA r2 a1079
SALTAC r3 0
SARIE r2 r3 !nein
SALTAC r2 30
SALTAC r3 4
SALTA r4 a1079
SCA r3 r4

TINE r3 a1078
SALTAC r4 20
ADN r4 r3

TINESSUS r9 r4
BAGASISTEMU

!nein
DEC r2
TINE r2 a1079

CADRU
SARI !simulare