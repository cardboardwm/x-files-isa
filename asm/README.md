# Limbajul de asamblare Fișierele-X

Probabil primul limbaj de asamblare în limba română.

## Generalități

Whitespace-ul este ignorat.

Comentariile încep cu `#` și ocupă toată linia.

Regiștrii se numesc:

- `r1, r2, r3, r4, r5, r6` - regiștrii generali
- `r8` - registrul pentru tastatură

## Secțiunea de date

Este prima secțiune a fișierului, și începe cu o linie pe care scrie `.date`.
Pe liniile următoare vor fi declarate simboluri care reprezintă în cod primul octet al secvenței de bytes.

Simbolurile încep cu caracterul `$`, urmate de o denumire scrisă cu majuscule și `_`.

Secvența de bytes poate fi fie un string ascii, între ghilimele (`"`), sau o secvență hexa, între slash-uri ('/').

Exemplu de secțiune de date:

```
.date
$MESAJ_INTRODUCERE "Buna, lume!"
$COD_MAGIC /1c 2b 3d/
```

## Secțiunea de cod

Începe cu o linie pe care scrie `.cod`.

Instrucțiunile se scriu cu litere mici, argumentele se dau cu spațiu.

Argumentele pot fi:
* regiștri, cum sunt notați mai sus
* adrese de memorie, încep cu litera `a`, urmată de 4 caractere hexa, literele sunt mari (`a6A3E`).
  * pot fi și simboluri de date (`$COD_MAGIC`).
* constante - numere întregi

O etichetă începe cu `!` și este urmată de un nume format din litere mici, cifre și `_`.

Instrucțiunile sunt următoarele:

* `SALTA <registru> <adresa>` - încarcă octetul de la adresă în registru;
* `SALTAC <registru> <constanta>` - pune constanta în registru;
* `SALTAS <registru special> <registru 1> <registru 2>` - încarcă registrul special în alți doi regiștri
* `SALTASSUS <registru special> <registru>` - încarcă biții de sus ai registrului special în registru
* `SALTASJOS <registru special> <registru>` - încarcă biții de jos ai registrului special în registru
* `SALTAMEM <registru high> <registru low> <registru destinatie>`
* `TINE <registru> <adresa>` - stochează octetul din registru în adresă;
* `TINEMEM <registru high> <registru low> <registru destinatie>`
* `ADN <registru 1> <registru 2>` - adună
* `SCA <registru 1> <registru 2>` - scade
* `IMP <registru 1> <registru 2>` - imparte
* `MUL <registru 1> <registru 2>` - inmulteste
* `SI <registru 1> <registru 2>`
* `SAU <registru 1> <registru 2>`
* `XAU <registru 1> <registru 2>`
* `IMPS <registru 1> <registru 2>` - left shift
* `IMPD <registru 1> <registru 2>` - right shift
* `MOD <registru 1> <registru 2>`
* `INC <registru>` - incrementează
* `DEC <registru>` - decrementează
* `SARI <adresa>` - jump la adresă
* `SARE <registru 1> <registru 2> <etichetă>` - jump la etichetă dacă regiștrii sunt egali
* `SARMI <registru 1> <registru 2> <etichetă>` - jump la etichetă dacă registrul 1 < registrul 2
* `SARMA <registru 1> <registru 2> <etichetă>` - jump la etichetă dacă registrul 1 > registrul 2
* `SARIE <registru 1> <registru 2> <etichetă>` - jump la etichetă dacă registrul 1 != registrul 2
* `SARMIE <registru 1> <registru 2> <etichetă>` - jump la etichetă dacă registrul 1 <= registrul 2
* `SARMAE <registru 1> <registru 2> <etichetă>` - jump la etichetă dacă registrul 1 >= registrul 2
* `DESSPIR <registru x> <registru y> <adresă>` - desenează spiridușul de la o adresă de memorie la coordonatele (x, y) pe ecran
* `DESSPIRAN <registru x> <registru y> <adresă>` - desenează spiridușul monocrom de la o adresă de memorie la coordonatele (x, y) pe ecran
* `STERGE` - șterge ecranul
* `CADRU` - debug - începe un nou cadru
* `INTST` - încarcă starea tastelor în regiștrii speciali
* `NIMEREALA <registru>` - random
* `AFRE <registru>` - debug - afișează valoarea registrului
* `AFMEM <adresă>` - debug - afișează valoarea celulei de memorie

Exemplu: factorial

```
.date
# facem N factorial
$N /5/
.cod
    SALTAC r1 1
    SALTAC r2 1
    SALTA r3 $N
!bucla
    SARMA r1 r3 !sf_bucla

    MUL r2 r1
    INC r1
    SARI !bucla
!sf_bucla
    AFRE r1
```
