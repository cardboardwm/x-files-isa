cmake_minimum_required(VERSION 3.9)
project(asn)

set(CMAKE_CXX_STANDARD 20)

add_executable(asm asm.cpp)