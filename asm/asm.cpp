#include <cstdio>
#include <cstring>
#include <vector>
#include <string>
#include <cstdint>
#include <cctype>
#include <iostream>
#include <iomanip>
#include <unordered_map>
#include <variant>
#include <typeinfo>
#include <type_traits>
#include <cassert>
#include <array>

using Opcode = std::uint32_t;
const int ROM_SIZE = 4096;

char current_char = 0;

struct DataSection {
    struct Symbol {
        std::string name;
        std::vector<uint8_t> bytes;
    };

    std::unordered_map<std::string, Symbol> symbols;
};

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

struct CodeSection {
    using Register = uint8_t;
    using Constant = uint8_t;
    struct Label {
        std::string name;
    };
    struct SymbolRef {
        std::string name;
    };
    struct ConstAddress {
        uint16_t addr;
    };
    using Address = std::variant<ConstAddress, SymbolRef>;
    using JumpLocation = std::variant<Label, Address>;

    struct Invalid {
    };

    // INST is instruction id
    // LEN is length of opcode in bytes

    // 0x0
    struct Salta {
        Register destination;
        Address source;
    };

    // 0x2
    struct SaltaC {
        Register destination;
        Constant constant;
    };

    // 0xDC
    struct SaltaS {
        Register special, r1, r2;
    };

    // 0xDE
    struct SaltaSSus {
        Register source;
        Register destination;
    };

    // 0xDF
    struct SaltaSJos {
        Register source;
        Register destination;
    };

    // 0x5
    struct SaltaMem {
        Register destination, r1, r2;
    };

    // 0x1
    struct Tine {
        Address destination;
        Register source;
    };

    // 0x6
    struct TineMem {
        Register r1, r2, source;
    };

    struct TineSSus {
        Register destination, source;
    };

    struct TineSJos {
        Register destination, source;
    };

    // 0xC0
    struct Adn {
        Register r1, r2;
    };

    // 0xC1
    struct Sca {
        Register r1, r2;
    };

    // 0xC2
    struct Imp {
        Register r1, r2;
    };

    // 0xC3
    struct Mul {
        Register r1, r2;
    };

    // 0xC4
    struct Si {
        Register r1, r2;
    };

    // 0xC5
    struct Sau {
        Register r1, r2;
    };

    // 0xC6
    struct Xau {
        Register r1, r2;
    };

    // 0xC7
    struct Imps {
        Register r1, r2;
    };

    // 0xC8
    struct Impd {
        Register r1, r2;
    };

    // 0xC9
    struct Mod {
        Register r1, r2;
    };

    // 0x3
    struct Inc {
        Register r;
    };

    // 0x4
    struct Dec {
        Register r;
    };

    // 0xD0
    struct Sari {
        JumpLocation location;
    };

    // 0xD1
    struct SarE {
        Register r1, r2;
        JumpLocation location;
    };

    // 0xD2
    struct SarMI {
        Register r1, r2;
        JumpLocation location;
    };

    // 0xD3
    struct SarMA {
        Register r1, r2;
        JumpLocation location;
    };

    // 0xD4
    struct SarIE {
        JumpLocation location;
        Register r1, r2;
    };

    // 0xD5
    struct SarMIE {
        Register r1, r2;
        JumpLocation location;
    };

    // 0xD6
    struct SarMAE {
        Register r1, r2;
        JumpLocation location;
    };

    // 0xE0
    struct DesSpirAN {
        Register rx, ry;
        Address address;
    };

    // 0xE1
    struct DesSpir {
        Register rx, ry;
        Address address;
    };

    // 0xE2
    struct Sterge {
    };

    // 0xE4
    struct StergeCuloare {
        Register color;
    };

    // 0xE3
    struct Cadru {
    };

    // 0xEF
    struct InTst {
    };

    // 0xEE
    struct Nimereala {
        Register destination;
    };

    // 0xED
    struct BagaSistemu {

    };

    // 0xFE
    struct AfRe {
        Register r;
    };

    // 0xFD
    struct AfMem {
        Address address;
    };

    using Instruction = std::variant<
        Invalid,
        Salta,
        SaltaC,
        SaltaS,
        SaltaSSus,
        SaltaSJos,
        SaltaMem,
        Tine,
        TineMem,
        TineSSus,
        TineSJos,
        Adn,
        Sca,
        Imp,
        Mul,
        Si,
        Sau,
        Xau,
        Imps,
        Impd,
        Mod,
        Inc,
        Dec,
        Sari,
        SarE,
        SarMI,
        SarMA,
        SarIE,
        SarMIE,
        SarMAE,
        DesSpirAN,
        DesSpir,
        Sterge,
        StergeCuloare,
        Cadru,
        InTst,
        Nimereala,
        BagaSistemu,
        AfRe,
        AfMem
    >;


    using Statement = std::variant<Label, std::pair<std::string, Instruction>>;

    std::vector<Statement> statements;
};

struct InstructionData {
    uint16_t id, len;
};

using namespace std::string_literals;
using namespace std::string_view_literals;
const std::unordered_map<std::string_view, InstructionData> instruction_data = {
    {"SALTA"sv, {0x0, 3}},
    {"SALTAC"sv, {0x2, 2}},
    {"SALTAS"sv, {0xDC, 3}},
    {"SALTASSUS"sv, {0xDE, 2}},
    {"SALTASJOS"sv, {0xDF, 2}},
    {"SALTAMEM"sv, {0x5, 2}},
    {"TINE"sv, {0x1, 3}},
    {"TINEMEM"sv, {0x6, 2}},
    {"TINESSUS", {0xDA, 2}},
    {"TINESJOS", {0xDB, 2}},
    {"ADN"sv, {0xC0, 2}},
    {"SCA"sv, {0xC1, 2}},
    {"IMP"sv, {0xC2, 2}},
    {"MUL"sv, {0xC3, 2}},
    {"SI"sv, {0xC4, 2}},
    {"SAU"sv, {0xC5, 2}},
    {"XAU"sv, {0xC6, 2}},
    {"IMPS"sv, {0xC7, 2}},
    {"IMPD"sv, {0xC8, 2}},
    {"MOD"sv, {0xC9, 2}},
    {"INC"sv, {0x3, 1}},
    {"DEC"sv, {0x4, 1}},
    {"SARI"sv, {0xD0, 3}},
    {"SARE"sv, {0xD1, 4}},
    {"SARMI"sv, {0xD2, 4}},
    {"SARMA"sv, {0xD3, 4}},
    {"SARIE"sv, {0xD4, 4}},
    {"SARMIE"sv, {0xD5, 4}},
    {"SARMAE"sv, {0xD6, 4}},
    {"DESSPIRAN"sv, {0xE0, 4}},
    {"DESSPIR"sv, {0xE1, 4}},
    {"STERGE"sv, {0xE2, 1}},
    {"STERGECULOARE", {0xE4, 2}},
    {"CADRU"sv, {0xE3, 1}},
    {"INTST"sv, {0xEF, 1}},
    {"NIMEREALA"sv, {0xEE, 2}},
    {"BAGASISTEMU", {0xED, 1}},
    {"AFRE"sv, {0xFE, 2}},
    {"AFMEM"sv, {0xFD, 3}},
};

void print_usage(char* prog_name)
{
    fprintf(stderr, "Usage: %s <input asm file> <output bin file>\n", prog_name);
    fprintf(stderr, "\tfiles can be \"-\" for stdin/stdout");
}

char read_char(FILE* fin)
{
    current_char = fgetc(fin);
    //printf("Reading %c\n", current_char);
    return current_char;
}

char peek_char(FILE* fin)
{
    if (current_char == 0) {
        read_char(fin);
    }
    return current_char;
}

void skip_ws(FILE* fin)
{
    while (isspace(peek_char(fin))) {
        read_char(fin);
    }
}


void read_comment(FILE* fin)
{
    skip_ws(fin);
    if (peek_char(fin) == '#') {
        char c;
        while ((c = read_char(fin)) != '\n' && c != EOF) {
            //printf("Reading '%c'\n", c);
        }
        if (c == '\n') {
            read_char(fin);
        }
    }
    skip_ws(fin);
    if (peek_char(fin) == '#') {
        read_comment(fin);
    }
}

std::string read_symbol_name(FILE* fin)
{
    std::string name;
    while (isupper(peek_char(fin)) || peek_char(fin) == '_') {
        name.push_back(peek_char(fin));
        read_char(fin);
    }

    return name;
}

std::vector<uint8_t> read_symbol_string(FILE* fin)
{
    std::vector<uint8_t> data;
    read_char(fin); // "
    while (isascii(peek_char(fin)) && peek_char(fin) != '"') {
        data.push_back(peek_char(fin));
        read_char(fin);
    }
    read_char(fin); // "

    return data;
}

std::vector<uint8_t> read_symbol_bytes(FILE* fin)
{
    std::vector<uint8_t> data;
    read_char(fin); // /
    while (peek_char(fin) != '/') {
        skip_ws(fin);
        uint8_t val = 0;
        char digit = peek_char(fin);
        if ('0' <= digit && digit <= '9') {
            val = (digit - '0') * 16;
        } else if ('a' <= digit && digit <= 'f') {
            val = (digit - 'a' + 10) * 16;
        }
        digit = read_char(fin);
        if ('0' <= digit && digit <= '9') {
            val += digit - '0';
        } else if ('a' <= digit && digit <= 'f') {
            val += digit - 'a' + 10;
        }
        read_char(fin);
        data.push_back(val);
    }
    read_char(fin); // /
    return data;
}

DataSection::Symbol read_symbol(FILE* fin)
{
    DataSection::Symbol sym;
    read_comment(fin);
    read_char(fin); // $
    sym.name = read_symbol_name(fin);
    skip_ws(fin);
    switch (peek_char(fin)) {
    case '"':
        sym.bytes = read_symbol_string(fin);
        break;
    case '/':
        sym.bytes = read_symbol_bytes(fin);
        break;
    }
    skip_ws(fin);

    return sym;
}

DataSection read_data_section(FILE* fin)
{
    DataSection data_section;
    read_comment(fin);

    // .date
    for (int i = 0; i < 5; i++) {
        read_char(fin);
    }
    read_comment(fin);
    while (peek_char(fin) != '.') {
        read_comment(fin);
        auto sym = read_symbol(fin);
        data_section.symbols.insert({sym.name, sym});
        read_comment(fin);
    }

    return data_section;
}

CodeSection::Label read_label(FILE* fin);

CodeSection::Address read_address(FILE* fin)
{
    skip_ws(fin);
    if (peek_char(fin) == 'a') {
        read_char(fin); // a
        CodeSection::ConstAddress addr = {0};

        auto power = 16 * 16 * 16;
        for (int i = 0; i < 4; i++) {
            char digit = peek_char(fin);
            if ('0' <= digit && digit <= '9') {
                addr.addr += power * (digit - '0');
            } else if ('A' <= digit && digit <= 'F') {
                addr.addr += power * (digit - 'A' + 10);
            }
            power /= 16;
            read_char(fin);
        }

        fprintf(stderr, "read address %d\n", addr);
        return addr;
    } else if (peek_char(fin) == '$') {
        read_char(fin);
        return CodeSection::SymbolRef { read_symbol_name(fin) };
    }

    assert("Wrong address");
}

CodeSection::JumpLocation read_jump_location(FILE* fin)
{
    skip_ws(fin);
    if (peek_char(fin) == '!') {
        return read_label(fin);
    } else if (peek_char(fin) == '$' || peek_char(fin) == 'a') {
        return read_address(fin);
    }

    assert("Malformed jump location");
}

CodeSection::Register read_register(FILE* fin)
{
    skip_ws(fin);
    read_char(fin); // r
    CodeSection::Register reg = peek_char(fin);
    read_char(fin);
    return reg - '0' - 1;
}

CodeSection::Constant read_constant(FILE* fin)
{
    skip_ws(fin);
    CodeSection::Constant constant = 0;
    while (isdigit(peek_char(fin))) {
        constant = constant * 10 + peek_char(fin) - '0';
        read_char(fin);
    }

    return constant;
}

std::pair<std::string, CodeSection::Instruction> read_instruction(FILE* fin)
{
    read_comment(fin);
    std::string op;
    while (isupper(peek_char(fin))) {
        op.push_back(peek_char(fin));
        read_char(fin);
    }
    skip_ws(fin);

    CodeSection::Instruction ret = CodeSection::Invalid {};
    if (op == "SALTA") {
        CodeSection::Salta salta;
        salta.destination = read_register(fin);
        salta.source = read_address(fin);
        ret = salta;
    } else if (op == "SALTAC") {
        CodeSection::SaltaC saltac;
        saltac.destination = read_register(fin);
        saltac.constant = read_constant(fin);
        ret = saltac;
    } else if (op == "SALTAS") {
        CodeSection::SaltaS saltas;
        saltas.special = read_register(fin);
        saltas.r1 = read_register(fin);
        saltas.r2 = read_register(fin);
        ret = saltas;
    } else if (op == "SALTASSUS") {
        CodeSection::SaltaSSus saltassus;
        saltassus.source = read_register(fin);
        saltassus.destination = read_register(fin);
        ret = saltassus;
    } else if (op == "SALTASJOS") {
        CodeSection::SaltaSJos saltasjos;
        saltasjos.source = read_register(fin);
        saltasjos.destination = read_register(fin);
        ret = saltasjos;
    } else if (op == "SALTAMEM") {
        CodeSection::SaltaMem saltamem;
        saltamem.r1 = read_register(fin);
        saltamem.r2 = read_register(fin);
        saltamem.destination = read_register(fin);
        ret = saltamem;
    } else if (op == "TINE") {
        CodeSection::Tine tine;
        tine.source = read_register(fin);
        tine.destination = read_address(fin);
        ret = tine;
    } else if (op == "TINEMEM") {
        CodeSection::TineMem tinemem;
        tinemem.r1 = read_register(fin);
        tinemem.r2 = read_register(fin);
        tinemem.source = read_register(fin);
        ret = tinemem;
    } else if (op == "TINESSUS") {
        CodeSection::TineSSus tinessus;
        tinessus.destination = read_register(fin);
        tinessus.source = read_register(fin);
        ret = tinessus;
    } else if (op == "TINESJOS") {
        CodeSection::TineSJos tinesjos;
        tinesjos.destination = read_register(fin);
        tinesjos.source = read_register(fin);
        ret = tinesjos;
    } else if (op == "ADN") {
        CodeSection::Adn adn;
        adn.r1 = read_register(fin);
        adn.r2 = read_register(fin);
        ret = adn;
    } else if (op == "SCA") {
        CodeSection::Sca sca;
        sca.r1 = read_register(fin);
        sca.r2 = read_register(fin);
        ret = sca;
    } else if (op == "IMP") {
        CodeSection::Imp imp;
        imp.r1 = read_register(fin);
        imp.r2 = read_register(fin);
        ret = imp;
    } else if (op == "MUL") {
        CodeSection::Mul mul;
        mul.r1 = read_register(fin);
        mul.r2 = read_register(fin);
        ret = mul;
    } else if (op == "SI") {
        CodeSection::Si si;
        si.r1 = read_register(fin);
        si.r2 = read_register(fin);
        ret = si;
    } else if (op == "SAU") {
        CodeSection::Sau Sau;
        Sau.r1 = read_register(fin);
        Sau.r2 = read_register(fin);
        ret = Sau;
    } else if (op == "XAU") {
        CodeSection::Xau xau;
        xau.r1 = read_register(fin);
        xau.r2 = read_register(fin);
        ret = xau;
    } else if (op == "IMPS") {
        CodeSection::Imps imps;
        imps.r1 = read_register(fin);
        imps.r2 = read_register(fin);
        ret = imps;
    } else if (op == "IMPD") {
        CodeSection::Impd impd;
        impd.r1 = read_register(fin);
        impd.r2 = read_register(fin);
        ret = impd;
    } else if (op == "MOD") {
        CodeSection::Mod mod;
        mod.r1 = read_register(fin);
        mod.r2 = read_register(fin);
        ret = mod;
    } else if (op == "INC") {
        CodeSection::Inc inc;
        inc.r = read_register(fin);
        ret = inc;
    } else if (op == "DEC") {
        CodeSection::Dec dec;
        dec.r = read_register(fin);
        ret = dec;
    } else if (op == "SARI") {
        CodeSection::Sari sari;
        sari.location = read_jump_location(fin);
        ret = sari;
    } else if (op == "SARE") {
        CodeSection::SarE sare;
        sare.r1 = read_register(fin);
        sare.r2 = read_register(fin);
        sare.location = read_jump_location(fin);
        ret = sare;
    } else if (op == "SARMA") {
        CodeSection::SarMA sarma;
        sarma.r1 = read_register(fin);
        sarma.r2 = read_register(fin);
        sarma.location = read_jump_location(fin);
        ret = sarma;
    } else if (op == "SARMI") {
        CodeSection::SarMI sarmi;
        sarmi.r1 = read_register(fin);
        sarmi.r2 = read_register(fin);
        sarmi.location = read_jump_location(fin);
        ret = sarmi;
    } else if (op == "SARIE") {
        CodeSection::SarIE sarie;
        sarie.r1 = read_register(fin);
        sarie.r2 = read_register(fin);
        sarie.location = read_jump_location(fin);
        ret = sarie;
    } else if (op == "SARMAE") {
        CodeSection::SarMAE sarmae;
        sarmae.r1 = read_register(fin);
        sarmae.r2 = read_register(fin);
        sarmae.location = read_jump_location(fin);
        ret = sarmae;
    } else if (op == "SARMIE") {
        CodeSection::SarMIE sarmie;
        sarmie.r1 = read_register(fin);
        sarmie.r2 = read_register(fin);
        sarmie.location = read_jump_location(fin);
        ret = sarmie;
    } else if (op == "DESSPIRAN") {
        CodeSection::DesSpirAN desspiran;
        desspiran.rx = read_register(fin);
        desspiran.ry = read_register(fin);
        desspiran.address = read_address(fin);
        ret = desspiran;
    } else if (op == "DESSPIR") {
        CodeSection::DesSpir desspir;
        desspir.rx = read_register(fin);
        desspir.ry = read_register(fin);
        desspir.address = read_address(fin);
        ret = desspir;
    } else if (op == "STERGE") {
        CodeSection::Sterge sterge;
        ret = sterge;
    } else if (op == "STERGECULOARE") {
        CodeSection::StergeCuloare stergeculoare;
        stergeculoare.color = read_register(fin);
        ret = stergeculoare;
    } else if (op == "CADRU") {
        CodeSection::Cadru cadru;
        ret = cadru;
    } else if (op == "INTST") {
        CodeSection::InTst intst;
        ret = intst;
    } else if (op == "NIMEREALA") {
        CodeSection::Nimereala nimereala;
        nimereala.destination = read_register(fin);
        ret = nimereala;
    } else if (op == "BAGASISTEMU") {
        ret = CodeSection::BagaSistemu{};
    } else if (op == "AFRE") {
        CodeSection::AfRe afre;
        afre.r = read_register(fin);
        ret = afre;
    } else if (op == "AFMEM") {
        CodeSection::AfMem afmem;
        afmem.address = read_address(fin);
        ret = afmem;
    } else {
        fprintf(stderr, "INVALID: '%s'", op.c_str());
        assert(false && "invalid instruction");
    }
    read_comment(fin);

    return { op, ret };
}

CodeSection::Label read_label(FILE* fin)
{
    read_comment(fin);
    read_char(fin); // !
    CodeSection::Label label;
    while (islower(peek_char(fin)) || peek_char(fin) == '_' || isdigit(peek_char(fin))) {
        label.name.push_back(peek_char(fin));
        read_char(fin);
    }
    read_comment(fin);

    return label;
}

CodeSection read_code_section(FILE* fin)
{
    CodeSection code_section;
    // .cod
    for (int i = 0; i < 4; i++) {
        read_char(fin);
    }
    skip_ws(fin);

    while (peek_char(fin) != EOF) {
        read_comment(fin);
        if (peek_char(fin) == '!') {
            code_section.statements.push_back(read_label(fin));
        } else {
            code_section.statements.push_back(read_instruction(fin));
        }
        read_comment(fin);
    }

    return code_section;
}

// returns a map from symbol name to start position in rom
std::unordered_map<std::string, uint16_t> store_data_into_rom(std::array<uint8_t, ROM_SIZE>& rom, const DataSection& data_section)
{
    std::unordered_map<std::string, uint16_t> map;
    int end_of_rom = ROM_SIZE;
    for (auto& pair : data_section.symbols) {
        auto& symbol = pair.second;
        int len = symbol.bytes.size();
        for (int i = 0; i < len; i++) {
            rom[end_of_rom - len + i] = symbol.bytes[i];
        }
        map.insert({symbol.name, end_of_rom - len});
        end_of_rom -= len;
    }
    assert(end_of_rom >= 0 && "Too much data");

    return map;
}

std::unordered_map<std::string, uint16_t> compute_jump_map(const CodeSection& code_section)
{
    std::unordered_map<std::string, uint16_t> jump_map;
    int pos = 0;
    for (auto& statement : code_section.statements) {
        std::visit(overloaded {
            [&](const CodeSection::Label& label) {
                jump_map.insert({label.name, pos});
            },
            [&](const std::pair<std::string, CodeSection::Instruction>& pair) {
                pos += instruction_data.at(pair.first).len;
                fprintf(stderr, "instruction %s has %d bytes\n", pair.first.c_str(), instruction_data.at(pair.first).len);
            },
        }, statement);
    }

    return jump_map;
}

uint16_t real_address(const std::unordered_map<std::string, uint16_t>& data_map, const CodeSection::Address& address)
{
    uint16_t ret = 0;
    std::visit(overloaded {
        [&](const CodeSection::ConstAddress& caddress) {
            ret = caddress.addr;
        },
        [&](const CodeSection::SymbolRef& symbol_ref) {
            ret = data_map.at(symbol_ref.name);
        },
    }, address);

    return ret;
}

uint16_t jump_address(const std::unordered_map<std::string, uint16_t>& jump_map, const std::unordered_map<std::string, uint16_t>& data_map, const CodeSection::JumpLocation& jump_location)
{
    uint16_t ret = 0;
    std::visit(overloaded {
        [&](const CodeSection::Label& label) {
            ret = jump_map.at(label.name);
        },
        [&](const CodeSection::Address& address) {
            ret = real_address(data_map, address);
        }
    }, jump_location);

    return ret;
}

void store_code_into_rom(std::array<uint8_t, ROM_SIZE>& rom, const CodeSection& code_section, const std::unordered_map<std::string, uint16_t>& data_map, const std::unordered_map<std::string, uint16_t>& jump_map)
{
    int pos = 0;
    for (auto& statement : code_section.statements) {
        std::visit( overloaded{
            [](const CodeSection::Label& label) {},
            [&](const std::pair<std::string, CodeSection::Instruction>& pair) {
                auto& instruction = pair.second;
                const auto& data = instruction_data.at(pair.first);
                std::visit(overloaded{
                    [&](const CodeSection::Salta& salta) {
                        uint16_t addr = real_address(data_map, salta.source);
                        rom[pos+0] = data.id << 4 | salta.destination;
                        rom[pos+1] = (addr & 0xFF00) >> 8;
                        rom[pos+2] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SaltaC& saltac) {
                        rom[pos+0] = data.id << 4 | saltac.destination;
                        rom[pos+1] = saltac.constant;
                    },
                    [&](const CodeSection::SaltaS& saltas) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = saltas.special << 4 | saltas.r1;
                        rom[pos+2] = saltas.r2 << 4;
                    },
                    [&](const CodeSection::SaltaSSus& saltassus) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = saltassus.destination << 4 | saltassus.source;
                    },
                    [&](const CodeSection::SaltaSJos& saltasjos) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = saltasjos.destination << 4 | saltasjos.source;
                    },
                    [&](const CodeSection::SaltaMem& saltamem) {
                        rom[pos+0] = data.id << 4 | saltamem.r1;
                        rom[pos+1] = saltamem.r2 << 4 | saltamem.destination;
                    },
                    [&](const CodeSection::Tine& tine) {
                        uint16_t addr = real_address(data_map, tine.destination);
                        rom[pos+0] = data.id << 4 | tine.source;
                        rom[pos+1] = (addr & 0xFF00) >> 8;
                        rom[pos+2] = addr & 0x00FF;
                    },
                    [&](const CodeSection::TineMem& tinemem) {
                        rom[pos+0] = data.id << 4 | tinemem.r1;
                        rom[pos+1] = tinemem.r2 << 4 | tinemem.source;
                    },
                    [&](const CodeSection::TineSSus& tinessus) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = tinessus.destination << 4 | tinessus.source;
                    },
                    [&](const CodeSection::TineSJos& tinesjos) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = tinesjos.destination << 4 | tinesjos.source;
                    },
                    [&](const CodeSection::Adn& adn) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = adn.r1 << 4 | adn.r2;
                    },
                    [&](const CodeSection::Sca& sca) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = sca.r1 << 4 | sca.r2;
                    },
                    [&](const CodeSection::Imp& imp) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = imp.r1 << 4 | imp.r2;
                    },
                    [&](const CodeSection::Mul& mul) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = mul.r1 << 4 | mul.r2;
                    },
                    [&](const CodeSection::Si& si) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = si.r1 << 4 | si.r2;
                    },
                    [&](const CodeSection::Sau& sau) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = sau.r1 << 4 | sau.r2;
                    },
                    [&](const CodeSection::Xau& xau) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = xau.r1 << 4 | xau.r2;
                    },
                    [&](const CodeSection::Imps& imps) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = imps.r1 << 4 | imps.r2;
                    },
                    [&](const CodeSection::Impd& impd) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = impd.r1 << 4 | impd.r2;
                    },
                    [&](const CodeSection::Mod& mod) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = mod.r1 << 4 | mod.r2;
                    },
                    [&](const CodeSection::Inc& inc) {
                        rom[pos+0] = data.id << 4 | inc.r;
                    },
                    [&](const CodeSection::Dec& dec) {
                        rom[pos+0] = data.id << 4 | dec.r;
                    },
                    [&](const CodeSection::Sari& sari) {
                        uint16_t addr = jump_address(jump_map, data_map, sari.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = (addr & 0xFF00) >> 8;
                        rom[pos+2] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SarE& sare) {
                        uint16_t addr = jump_address(jump_map, data_map, sare.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = sare.r1 << 4 | sare.r2;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SarMI& sarmi) {
                        uint16_t addr = jump_address(jump_map, data_map, sarmi.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = sarmi.r1 << 4 | sarmi.r2;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SarMA& sarma) {
                        uint16_t addr = jump_address(jump_map, data_map, sarma.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = sarma.r1 << 4 | sarma.r2;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SarIE& sarie) {
                        uint16_t addr = jump_address(jump_map, data_map, sarie.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = sarie.r1 << 4 | sarie.r2;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SarMIE& sarmie) {
                        uint16_t addr = jump_address(jump_map, data_map, sarmie.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = sarmie.r1 << 4 | sarmie.r2;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::SarMAE& sarmae) {
                        uint16_t addr = jump_address(jump_map, data_map, sarmae.location);
                        rom[pos+0] = data.id;
                        rom[pos+1] = sarmae.r1 << 4 | sarmae.r2;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::DesSpirAN& desspiran) {
                        uint16_t addr = real_address(data_map, desspiran.address);
                        rom[pos+0] = data.id;
                        rom[pos+1] = desspiran.rx << 4 | desspiran.ry;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::DesSpir& desspir) {
                        uint16_t addr = real_address(data_map, desspir.address);
                        rom[pos+0] = data.id;
                        rom[pos+1] = desspir.rx << 4 | desspir.ry;
                        rom[pos+2] = (addr & 0xFF00) >> 8;
                        rom[pos+3] = addr & 0x00FF;
                    },
                    [&](const CodeSection::Sterge&) {
                        rom[pos+0] = data.id;
                    },
                    [&](const CodeSection::StergeCuloare& stergeculoare) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = stergeculoare.color << 4;
                    },
                    [&](const CodeSection::Cadru&) {
                        rom[pos+0] = data.id;
                    },
                    [&](const CodeSection::InTst&) {
                        rom[pos+0] = data.id;
                    },
                    [&](const CodeSection::Nimereala& nimereala) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = nimereala.destination << 4;
                    },
                    [&](const CodeSection::BagaSistemu&) {
                        rom[pos+0] = data.id;
                    },
                    [&](const CodeSection::AfRe& afre) {
                        rom[pos+0] = data.id;
                        rom[pos+1] = afre.r << 4;
                    },
                    [&](const CodeSection::AfMem& afmem) {
                        uint16_t addr = real_address(data_map, afmem.address);
                        rom[pos+0] = data.id;
                        rom[pos+1] = (addr & 0xFF00) >> 8;
                        rom[pos+2] = addr & 0x00FF;
                    },
                    [](const auto& arg) {
                        fprintf(stderr, "%s\n", typeid(arg).name());
                        assert(false && "not implemented");
                    },
                }, instruction);

                pos += data.len;
            },
        }, statement);
    }
}

int main(int argc, char* argv[])
{
    if (argc != 3) {
        print_usage(argv[0]);
        return 1;
    }

    std::array<uint8_t, ROM_SIZE> rom;
    for (int i = 0; i < ROM_SIZE; i++) {
        rom[i] = 0xff;
    }
    FILE* fin;
    FILE* fout;

    if (strcmp(argv[1], "-") == 0) {
        fin = stdin;
    } else {
        fin = fopen(argv[1], "r");
    }
    if (!fin) {
        fprintf(stderr, "Couldn't open file %s\n", argv[1]);
        return 1;
    }

    if (strcmp(argv[2], "-") == 0) {
        fout = freopen(NULL, "wb", stdout);
    } else {
        fout = fopen(argv[2], "wb");
    }
    if (!fout) {
        fprintf(stderr, "Couldn't open file %s\n", argv[2]);
        return 1;
    }

    auto data_section = read_data_section(fin);
    /**/
    for (auto& pair : data_section.symbols) {
        auto& sym = pair.second;
        fprintf(stderr, "Symbol %s:\n", sym.name.c_str());
        fprintf(stderr, "\t");
        for (auto byte : sym.bytes) {
            std::cerr << std::hex << std::setfill('0') << std::setw(2) << (int)byte << ' ';
        }
        std::cerr << std::endl;
    }
    auto code_section = read_code_section(fin);
    for (auto& statement : code_section.statements) {
        std::visit( overloaded{
            [](CodeSection::Label& label) { fprintf(stderr, "Label %s\n", label.name.c_str()); },
            [](std::pair<std::string, CodeSection::Instruction>& instruction) {
                std::visit([](auto&& arg) {
                    fprintf(stderr, "Instruction %s\n", typeid(arg).name());
                }, instruction.second);
            }
        }, statement);
    }
    auto data_map = store_data_into_rom(rom, data_section);
    for (auto& pair : data_map) {
        fprintf(stderr, "%s is at position %d\n", pair.first.c_str(), pair.second);
    }
    auto jump_map = compute_jump_map(code_section);
    for (auto& pair : jump_map) {
        fprintf(stderr, "jump %s is at pos %d\n", pair.first.c_str(), pair.second);
    }
    store_code_into_rom(rom, code_section, data_map, jump_map);

    fwrite(rom.data(), sizeof(uint8_t), ROM_SIZE, fout);
    fclose(fout);

    return 0;
}
