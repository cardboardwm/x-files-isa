# Note about this project

This project is the result after 24 hours of our (@alexge50 and @tudorr) hard work during the
InfoEducație Open 2020 hackathon, part of the [InfoEducație - Digital Innovation and
Creativity Olympiad](https://infoeducatie.ro), a software development contest for high school students
sponsored by the [Ministry of Education and Research of Romania](https://edu.ro). Cardboard was our main
submission for the contest, followed by this project made for the hackathon round.

It consists of an ISA (documented in this file), an assembly language for the ISA (the first assembly language in Romanian, probably),
an assembler, an emulator and two demo "games" (rain and "DOOM").

We won the contest and the hackathon 😄!

The original README is the following:

# x-files-isa
`x-files-isa` este o arhitectura necunoscuta gasita de un programator anonim pe un harddisk cumparat de pe OLX. Nu se stie daca a existat vreodata o bucata de circuit sau chip care sa implementeze aceasta arhitectura. Emulatorul si assemblerul sunt menite sa implementeze aceasta arhitectura obscura. `(PS: arhitectura este originala)`

## Descriere sisteme
### Memory Layout-ul
Layoutul memorie este asa cum este prezentat in urmatoarea figura:
```
|----------------------------|
|ROM    |  [0x0000, 0x1000)  |
|RAM    |  [0x1000, 0x1080)  |
|VRAM   |  [0x1080, 0x2080)  |
\----------------------------/
```
Zona de memorie `ROM` este protejata la scriere - in prezent scrierile in rom sunt inlocuite cu `no-op`. Zonele `RAM` si `VRAM` pot fi atat citite cat si scrise.

### Sistemul Grafic
Memoria grafica (`VRAM`) reprezinta tot bufferul ecranului. Fiecare pixel ocupa jumatate de byte (`0x0 - 0xF`), astfel incat, sunt permise 16 culori, dintr-o paleta predefinita, asa cum se poate observa aici:

Exista 2 tipuri de spriteuri:
* sprite-ul "normal" care este reprezentat in memorie asemenea bufferului ecranului (fiecare pixel ocupa jumatate de byte)
* sprite-ul "black and white" care este un sprite care poate retine 2 culori. Acest sprite este retinut in memorie ca o insiruire de bits, fiecare bit fiind corespunzator unui pixel, insa instructiunea care deseneaza acest tip de sprite permite setarea a 2 culori care preiaul rolul negrului si albului, prin registrul `r5`;

### Sistemul de Input
Arhitectura este prevazuta cu un registru de I/O (`r8`), de 2 bytes, care poate fi incarcat cu statutul butoanelor (daca acestea sunt apasate sau nu). Este necesara apelarea unei instructiuni ajutatoare care sa trimita un semnal prin care sistemul este alertat sa umple registrul de I/O cu statutul curent al tastelor.

### Sistemul Audio
Arhitectura procesorului este prevazuta cu un registru audio si o instructiune speciala care trimite registrul la procesorul audio. Procesorul audio suporta polifonie de 2 voci, fiecare byte al registrului desemnand ce nota este apasata curent. Notele sunt marcate asemenea MIDI, pentru referinta: https://newt.phys.unsw.edu.au/jw/notes.html.

### Instructiuni
Semnificatie notatii in encoding:
* fiecare litera inseamna jumatate de byte
* I/II - "id-ul" instructiunii
* DD - date de un byte
* R - "id-ul" unui registru
* XXXX - zona de memorie
* P - byte pentru padding; nefolosit
* registrii parametrii ai instructiunilor sunt numerotati de la stanga la dreapta encodarii cu cifre de la 1 la 3 maxim: de exemplu `IRRRR` este o instructiune care admite 3 registrii: R1, R2, R3; R1 fiind cel mai din stanga, iar R3 cel mai din dreapta

O parte din instructiuni:

```
0x0RXXXX - load; copiaza byte-ul la adresa indicata in registrul R1
0x1RXXXX - store; copiaza valoarea registrului R1 in zona de memorie indicata
0x2RDD - load const; copiaza valoarea DD in registrul R1
0xDCRRRP - load special register; copiaza valoarea registrului special R3, in registrii R1 (high byte) si R2 (low byte)
0xDERR - load special register high byte; copiaza `high byte`-ul registrului special R2 in R1
0xDFRR - load special register low byte; copiaza `low byte`-ul registrului special R2 in R1
0xDARR - store special register high byte; copiaza R1 in `high byte`-ul registrului special R2
0xDBRR - store special register low byte; copiaza R1 in `low byte`-ul registrului special R2
0x5RRR - load memory from registers; interpreteaza R1 (high byte) si R2 (low byte) ca o adresa si copiaza memoria de la acea adresa in R3
0x6RRR - store memory from registers; interpreteaza R1 (high byte) si R2 (low byte) ca o adresa si copiaza R3 in memoria de la acea adresa
0xC0RR - `R1 += R2`
0xC1RR - `R1 -= R2`
0xC2RR - `R1 /= R2`
0xC3RR - `R1 *= R2`
0xC9RR - `R1 %= R2`
0xC4RR - `R1 &= R2`
0xC5RR - `R1 |= R2`
0xC6RR - `R1 ^= R2`
0xC7RR - `R1 <<= R2`
0xC8RR - `R1 >>= R2`
0x3R - `R1 ++`
0x4R - `R1 --`
0xD0XXXX - jump la adresa indicata
0xD1RRXXXX - jump conditionat la adresa indicata: `R1 == R2`
0xD2RRXXXX - jump conditionat la adresa indicata: `R1 < R2`
0xD3RRXXXX - jump conditionat la adresa indicata: `R1 > R2`
0xD4RRXXXX - jump conditionat la adresa indicata: `R1 != R2`
0xD5RRXXXX - jump conditionat la adresa indicata: `R1 <= R2`
0xD6RRXXXX - jump conditionat la adresa indicata: `R1 >= R2`
0xE1RRXXXX - deseneaza sprite-ul "normal" aflat la adresa indicata la pozitia (x: R1, y: R2)
0xE0RRXXXX - deseneaza sprite-ul "bw" aflat la adresa indicata la pozitia (x: R1, y: R2)
0XE2 - curata bufferul ecranului
0XE4RP - curata bufferul ecranului cu culoarea indicata de R1
0XE3 - "frame" - trimite frame la restul sistemului
0xEF - incarca statutul tastelor in registrul de I/O
0xEERP - incarca un numar random in registrul R1
0XED - trimite procesorului audio registrul audio
0xFERP - afiseaza la portul serial (in cazul emulatorului la stdout) valorea registrului R1
0xFDXXXX - afiseaza la portul serial (in cazul emulatorului la stdout) valoarea aflata la adresa indicata
0xFF - instructiune ilegala; procesorul/emulatorul se reseteaza
```

Toate instructiunile, impreuna cu descrierile lor, pot fi vazute in fisierul `include/instructions.h`.
