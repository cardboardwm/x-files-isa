{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    gcc10
    clang
    cmake
    gnumake
  ];

  nativeBuildInputs = with pkgs; [
    sfml
    libjack2
    alsaLib
  ];
}
